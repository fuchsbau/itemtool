package de.fuchspfoten.itemtool.command.recipe;

import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.fuchslib.command.LeafCommand;
import de.fuchspfoten.itemtool.ItemStorage;
import de.fuchspfoten.itemtool.ItemToolPlugin;
import org.bukkit.Bukkit;
import org.bukkit.NamespacedKey;
import org.bukkit.command.CommandSender;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;

import java.util.ArrayList;
import java.util.List;

/**
 * /i recipe shaped KEY OUT SHAPE IN... registers a shaped recipe.
 */
public class ItemToolRecipeShapedCommand extends LeafCommand {

    /**
     * Constructor.
     */
    protected ItemToolRecipeShapedCommand() {
        super("itemtool.help.recipe.shaped", "itemtool.use");
        Messenger.register("itemtool.recipe.notFound");
        Messenger.register("itemtool.recipe.registered");
    }

    @Override
    protected void invoke(final CommandSender sender, final String[] args) {
        // Syntax check.
        if (args.length < 4) {
            Messenger.send(sender, "itemtool.help.recipe.shaped");
            return;
        }
        final String key = args[0].toLowerCase();
        final String keyOut = args[1].toLowerCase();
        final String shape = args[2].toLowerCase();

        // Fetch the ingredients and the result.
        final ItemStorage storage = ItemToolPlugin.getSelf().getItemStorage();
        final List<ItemStack> ingredients = new ArrayList<>();
        for (int i = 3; i < args.length; i++) {
            final String keyIn = args[i].toLowerCase();
            final ItemStack ingredient = storage.load(keyIn);
            if (ingredient == null) {
                Messenger.send(sender, "itemtool.recipe.notFound");
                return;
            }
            ingredients.add(ingredient);
        }

        final ItemStack out = storage.load(keyOut);
        if (out == null) {
            Messenger.send(sender, "itemtool.recipe.notFound");
            return;
        }

        // Store the recipe.
        final String[] shapes = shape.split(",");
        final ShapedRecipe recipe = new ShapedRecipe(new NamespacedKey(ItemToolPlugin.getSelf(), key), out);
        recipe.shape(shapes);
        for (int i = 0; i < ingredients.size(); i++) {
            recipe.setIngredient((char) ('1' + i), ingredients.get(i).getData());
        }

        Bukkit.addRecipe(recipe);
        ItemToolPlugin.getSelf().getCraftingStorage().store(key, recipe);
        ItemToolPlugin.getSelf().getCraftingStorage().save();
        Messenger.send(sender, "itemtool.recipe.registered");
    }
}
