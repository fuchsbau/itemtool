package de.fuchspfoten.itemtool.command;

import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.fuchslib.PlayerHelper;
import de.fuchspfoten.fuchslib.command.LeafCommand;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 * /i damage AMOUNT sets the item damage.
 */
public class ItemToolDamageCommand extends LeafCommand {

    /**
     * Constructor.
     */
    protected ItemToolDamageCommand() {
        super("itemtool.help.damage", "itemtool.use");
        Messenger.register("itemtool.applied");
        Messenger.register("itemtool.noItem");
    }

    @Override
    protected void invoke(final CommandSender sender, final String[] args) {
        final Player player = PlayerHelper.ensurePlayer(sender);
        if (player == null) {
            return;
        }

        // Syntax check.
        if (args.length != 1) {
            Messenger.send(sender, "itemtool.help.damage");
            return;
        }

        // Get the new durability value.
        final short newDamage;
        try {
            newDamage = Short.parseShort(args[0]);
        } catch (final NumberFormatException ex) {
            return;
        }

        // Get the item to edit.
        final ItemStack itemInHand = player.getInventory().getItemInMainHand();
        if (itemInHand == null || itemInHand.getType() == Material.AIR) {
            Messenger.send(sender, "itemtool.noItem");
            return;
        }

        // Change the durability.
        itemInHand.setDurability(newDamage);
        Messenger.send(sender, "itemtool.applied");
    }
}
