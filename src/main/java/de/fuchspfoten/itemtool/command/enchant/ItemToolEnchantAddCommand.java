package de.fuchspfoten.itemtool.command.enchant;

import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.fuchslib.PlayerHelper;
import de.fuchspfoten.fuchslib.command.LeafCommand;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.Arrays;

/**
 * /i enchant add ENCH LEVEL adds an enchantment to an item.
 */
public class ItemToolEnchantAddCommand extends LeafCommand {

    /**
     * Constructor.
     */
    protected ItemToolEnchantAddCommand() {
        super("itemtool.help.enchant.add", "itemtool.use");
        Messenger.register("itemtool.applied");
        Messenger.register("itemtool.enchant.invalidLevel");
        Messenger.register("itemtool.enchant.unknown");
        Messenger.register("itemtool.noItem");
    }

    @Override
    protected void invoke(final CommandSender sender, final String[] args) {
        final Player player = PlayerHelper.ensurePlayer(sender);
        if (player == null) {
            return;
        }

        // Syntax check.
        if (args.length != 2) {
            Messenger.send(sender, "itemtool.help.enchant.add");
            return;
        }

        // Get the item to edit.
        final ItemStack itemInHand = player.getInventory().getItemInMainHand();
        if (itemInHand == null || itemInHand.getType() == Material.AIR) {
            Messenger.send(sender, "itemtool.noItem");
            return;
        }

        // Get the enchantment.
        final Enchantment ench = Arrays.stream(Enchantment.values())
                .filter(e -> e.getName().equalsIgnoreCase(args[0]))
                .findAny().orElse(null);
        if (ench == null) {
            Messenger.send(sender, "itemtool.enchant.unknown");
            return;
        }

        // Get the level.
        final int level;
        try {
            level = Integer.parseInt(args[1]);
            if (level <= 0) {
                Messenger.send(sender, "itemtool.enchant.invalidLevel");
                return;
            }
        } catch (final NumberFormatException ex) {
            return;
        }

        // Add the enchantment.
        itemInHand.addUnsafeEnchantment(ench, level);
        Messenger.send(sender, "itemtool.applied");
    }
}
