package de.fuchspfoten.itemtool.command.custom;

import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.fuchslib.command.LeafCommand;
import de.fuchspfoten.fuchslib.item.CustomItems;
import org.bukkit.command.CommandSender;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * /i custom list lists available {@link de.fuchspfoten.fuchslib.item.CustomItems}.
 */
public class ItemToolCustomListCommand extends LeafCommand {

    /**
     * Constructor.
     */
    protected ItemToolCustomListCommand() {
        super("itemtool.help.custom.list", "itemtool.use");
        Messenger.register("itemtool.custom.list");
    }

    @Override
    protected void invoke(final CommandSender sender, final String[] args) {
        // Show the available enchantments.
        Messenger.send(sender, "itemtool.custom.list", String.join(", ",
                Arrays.stream(CustomItems.values())
                        .map(CustomItems::name)
                        .map(String::toLowerCase)
                        .collect(Collectors.toList())));
    }
}
