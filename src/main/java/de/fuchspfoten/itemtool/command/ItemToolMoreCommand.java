package de.fuchspfoten.itemtool.command;

import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.fuchslib.PlayerHelper;
import de.fuchspfoten.fuchslib.command.LeafCommand;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 * /i more maximizes the held stack.
 */
public class ItemToolMoreCommand extends LeafCommand {

    /**
     * Constructor.
     */
    protected ItemToolMoreCommand() {
        super("itemtool.help.more", "itemtool.use");
        Messenger.register("itemtool.applied");
        Messenger.register("itemtool.noItem");
    }

    @Override
    protected void invoke(final CommandSender sender, final String[] args) {
        final Player player = PlayerHelper.ensurePlayer(sender);
        if (player == null) {
            return;
        }

        // Syntax check.
        if (args.length != 0) {
            Messenger.send(sender, "itemtool.help.more");
            return;
        }

        // Get the item to edit.
        final ItemStack itemInHand = player.getInventory().getItemInMainHand();
        if (itemInHand == null || itemInHand.getType() == Material.AIR) {
            Messenger.send(sender, "itemtool.noItem");
            return;
        }

        // Change the durability.
        itemInHand.setAmount(64);
        Messenger.send(sender, "itemtool.applied");
    }
}
