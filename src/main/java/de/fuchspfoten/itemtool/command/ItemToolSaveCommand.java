package de.fuchspfoten.itemtool.command;

import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.fuchslib.PlayerHelper;
import de.fuchspfoten.fuchslib.command.LeafCommand;
import de.fuchspfoten.itemtool.ItemToolPlugin;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 * /i save KEY saves the item in hand.
 */
public class ItemToolSaveCommand extends LeafCommand {

    /**
     * Constructor.
     */
    protected ItemToolSaveCommand() {
        super("itemtool.help.save", "itemtool.use");
        Messenger.register("itemtool.itemSaved");
        Messenger.register("itemtool.noItem");
    }

    @Override
    protected void invoke(final CommandSender sender, final String[] args) {
        final Player player = PlayerHelper.ensurePlayer(sender);
        if (player == null) {
            return;
        }

        // Syntax check.
        if (args.length != 1) {
            Messenger.send(sender, "itemtool.help.save");
            return;
        }
        final String key = args[0].toLowerCase();

        final ItemStack itemInHand = player.getInventory().getItemInMainHand();
        if (itemInHand == null || itemInHand.getType() == Material.AIR) {
            Messenger.send(sender, "itemtool.noItem");
            return;
        }

        final ItemStack singular = itemInHand.clone();
        singular.setAmount(1);
        ItemToolPlugin.getSelf().getItemStorage().store(key, singular);
        ItemToolPlugin.getSelf().getItemStorage().save();
        Messenger.send(sender, "itemtool.itemSaved");
    }
}
