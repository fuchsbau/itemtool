package de.fuchspfoten.itemtool.command;

import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.fuchslib.PlayerHelper;
import de.fuchspfoten.fuchslib.command.LeafCommand;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * /i get QUERY acquires the queried item.
 */
public class ItemToolGetCommand extends LeafCommand {

    /**
     * The pattern for sanitizing the query.
     */
    private static final Pattern QUERY_SANITIZE_PATTERN = Pattern.compile("_-");

    /**
     * Maps strings to corresponding data values.
     */
    private static final Map<String, Short> DATA_MAPPINGS = new HashMap<>();

    static {
        DATA_MAPPINGS.put("white", (short) 0);
        DATA_MAPPINGS.put("oak", (short) 0);

        DATA_MAPPINGS.put("orange", (short) 1);
        DATA_MAPPINGS.put("spruce", (short) 1);

        DATA_MAPPINGS.put("magenta", (short) 2);
        DATA_MAPPINGS.put("birch", (short) 2);

        DATA_MAPPINGS.put("lightblue", (short) 3);
        DATA_MAPPINGS.put("jungle", (short) 3);

        DATA_MAPPINGS.put("yellow", (short) 4);
        DATA_MAPPINGS.put("acacia", (short) 4);

        DATA_MAPPINGS.put("lime", (short) 5);
        DATA_MAPPINGS.put("lightgreen", (short) 5);
        DATA_MAPPINGS.put("darkoak", (short) 5);

        DATA_MAPPINGS.put("pink", (short) 6);

        DATA_MAPPINGS.put("grey", (short) 7);
        DATA_MAPPINGS.put("darkgrey", (short) 7);
        DATA_MAPPINGS.put("gray", (short) 7);
        DATA_MAPPINGS.put("darkgray", (short) 7);

        DATA_MAPPINGS.put("lightgrey", (short) 8);
        DATA_MAPPINGS.put("lightgray", (short) 8);

        DATA_MAPPINGS.put("cyan", (short) 9);

        DATA_MAPPINGS.put("purple", (short) 10);
        DATA_MAPPINGS.put("violet", (short) 10);

        DATA_MAPPINGS.put("blue", (short) 11);
        DATA_MAPPINGS.put("darkblue", (short) 11);

        DATA_MAPPINGS.put("brown", (short) 12);

        DATA_MAPPINGS.put("green", (short) 13);
        DATA_MAPPINGS.put("darkgreen", (short) 13);

        DATA_MAPPINGS.put("red", (short) 14);

        DATA_MAPPINGS.put("black", (short) 15);
    }

    /**
     * Read a material from a string query.
     *
     * @param query The string query.
     * @return The material, or null if none matched.
     */
    private static Material readMaterial(final String query) {
        // Try finding a material match.
        for (final Material material : Material.values()) {
            final String sanitizedMaterial =
                    QUERY_SANITIZE_PATTERN.matcher(material.name().toLowerCase()).replaceAll("");
            if (sanitizedMaterial.equals(query)) {
                return material;
            }
        }

        // Try parsing the query as an ID.
        try {
            final int parsed = Integer.parseInt(query);
            return Material.getMaterial(parsed);
        } catch (final NumberFormatException ex) {
            return null;
        }
    }

    /**
     * Read a data value from a string query.
     *
     * @param query The string query.
     * @return The data value.
     */
    private static short readData(final String query) {
        if (DATA_MAPPINGS.containsKey(query)) {
            return DATA_MAPPINGS.get(query);
        }

        try {
            return Short.parseShort(query);
        } catch (final NumberFormatException ex) {
            return (short) 0;
        }
    }

    /**
     * Constructor.
     */
    protected ItemToolGetCommand() {
        super("itemtool.help.get", "itemtool.use");
        Messenger.register("itemtool.notFound");
        Messenger.register("itemtool.received");
    }

    @Override
    protected void invoke(final CommandSender sender, final String[] args) {
        final Player player = PlayerHelper.ensurePlayer(sender);
        if (player == null) {
            return;
        }

        // Syntax check.
        if (args.length != 1) {
            Messenger.send(sender, "itemtool.help.get");
            return;
        }
        final String query = QUERY_SANITIZE_PATTERN.matcher(args[0].toLowerCase()).replaceAll("");
        final String[] split = query.split(":");

        // Determine the material and data.
        final Material type = readMaterial(split[0]);
        final short data = split.length == 2 ? readData(split[1]) : (short) 0;
        if (type == null) {
            Messenger.send(sender, "itemtool.notFound");
            return;
        }

        player.getInventory().addItem(new ItemStack(type, 1, data));
        Messenger.send(sender, "itemtool.received");
    }
}
