/*
 * Copyright (c) 2018. All Rights Reserved.
 */

package de.fuchspfoten.itemtool;

import de.fuchspfoten.itemtool.command.ItemToolCommand;
import lombok.Getter;
import org.bukkit.plugin.ServicePriority;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Plugin for being loaded by Bukkit.
 */
public class ItemToolPlugin extends JavaPlugin {

    /**
     * Singleton plugin instance.
     */
    private @Getter static ItemToolPlugin self;

    /**
     * The item storage.
     */
    private @Getter ItemStorage itemStorage;

    /**
     * The crafting recipe storage.
     */
    private @Getter CraftingStorage craftingStorage;

    @Override
    public void onDisable() {
        itemStorage.save();
        craftingStorage.save();
    }

    @Override
    public void onEnable() {
        self = this;

        // Create default config.
        getConfig().options().copyDefaults(true);
        saveConfig();

        // Create the item storage.
        itemStorage = new ItemStorage(getDataFolder());

        // Create the crafting storage and load recipes.
        craftingStorage = new CraftingStorage(getDataFolder());
        craftingStorage.loadAllRecipes();

        // Register services.
        getServer().getServicesManager().register(ItemStorage.class, itemStorage, this, ServicePriority.Normal);

        // Register commands.
        getCommand("itemtool").setExecutor(new ItemToolCommand());

        // Register listeners.
        // ...
    }
}
