package de.fuchspfoten.itemtool.command;

import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.fuchslib.command.LeafCommand;
import de.fuchspfoten.itemtool.ItemToolPlugin;
import org.bukkit.command.CommandSender;

/**
 * /i remove KEY removes the item with the given key.
 */
public class ItemToolRemoveCommand extends LeafCommand {

    /**
     * Constructor.
     */
    protected ItemToolRemoveCommand() {
        super("itemtool.help.remove", "itemtool.use");
        Messenger.register("itemtool.itemRemoved");
    }

    @Override
    protected void invoke(final CommandSender sender, final String[] args) {
        // Syntax check.
        if (args.length != 1) {
            Messenger.send(sender, "itemtool.help.remove");
            return;
        }
        final String key = args[0].toLowerCase();

        // Remove the item.
        ItemToolPlugin.getSelf().getItemStorage().remove(key);
        ItemToolPlugin.getSelf().getItemStorage().save();
        Messenger.send(sender, "itemtool.itemRemoved");
    }
}
