package de.fuchspfoten.itemtool;

import de.fuchspfoten.fuchslib.data.DataFile;

import java.util.function.BiConsumer;

/**
 * Updates the format of the item storage, if required.
 */
public class ItemStorageVersionUpdater implements BiConsumer<Integer, DataFile> {

    /**
     * The current version.
     */
    public static final int VERSION = 1;

    @Override
    public void accept(final Integer integer, final DataFile dataFile) {
        // Nothing to do.
    }
}
