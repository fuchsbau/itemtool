package de.fuchspfoten.itemtool.command.recipe;

import de.fuchspfoten.fuchslib.command.TreeCommand;

/**
 * /i recipe manages recipes.
 */
public class ItemToolRecipeCommand extends TreeCommand {

    /**
     * Constructor.
     */
    public ItemToolRecipeCommand() {
        super("itemtool.help.recipe.self");
        addSubCommand("furnace", new ItemToolRecipeFurnaceCommand());
        addSubCommand("remove", new ItemToolRecipeRemoveCommand());
        addSubCommand("reset", new ItemToolRecipeResetCommand());
        addSubCommand("shaped", new ItemToolRecipeShapedCommand());
        addSubCommand("shapeless", new ItemToolRecipeShapelessCommand());
    }
}
