package de.fuchspfoten.itemtool.command.recipe;

import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.fuchslib.command.LeafCommand;
import de.fuchspfoten.itemtool.ItemToolPlugin;
import org.bukkit.command.CommandSender;

/**
 * /i recipe remove KEY removes the recipe with the given key.
 */
public class ItemToolRecipeRemoveCommand extends LeafCommand {

    /**
     * Constructor.
     */
    protected ItemToolRecipeRemoveCommand() {
        super("itemtool.help.recipe.remove", "itemtool.use");
        Messenger.register("itemtool.recipe.removed");
    }

    @Override
    protected void invoke(final CommandSender sender, final String[] args) {
        // Syntax check.
        if (args.length != 1) {
            Messenger.send(sender, "itemtool.help.recipe.remove");
            return;
        }
        final String key = args[0].toLowerCase();

        // Remove the recipe.
        ItemToolPlugin.getSelf().getCraftingStorage().remove(key);
        ItemToolPlugin.getSelf().getCraftingStorage().save();
        Messenger.send(sender, "itemtool.recipe.removed");
    }
}
