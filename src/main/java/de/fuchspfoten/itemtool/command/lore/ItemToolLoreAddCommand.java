package de.fuchspfoten.itemtool.command.lore;

import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.fuchslib.PlayerHelper;
import de.fuchspfoten.fuchslib.command.LeafCommand;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

/**
 * /i lore add line adds a lore line.
 */
public class ItemToolLoreAddCommand extends LeafCommand {

    /**
     * Constructor.
     */
    protected ItemToolLoreAddCommand() {
        super("itemtool.help.lore.add", "itemtool.use");
        Messenger.register("itemtool.applied");
        Messenger.register("itemtool.noItem");
    }

    @Override
    protected void invoke(final CommandSender sender, final String[] args) {
        final Player player = PlayerHelper.ensurePlayer(sender);
        if (player == null) {
            return;
        }

        // Syntax check.
        if (args.length < 1) {
            Messenger.send(sender, "itemtool.help.lore.add");
            return;
        }

        // Parse the new lore line.
        final StringBuilder loreBuilder = new StringBuilder();
        for (final String arg : args) {
            loreBuilder.append(arg).append(' ');
        }
        final String loreLine = ChatColor.translateAlternateColorCodes('&', loreBuilder.toString().trim());

        // Get the item to edit.
        final ItemStack itemInHand = player.getInventory().getItemInMainHand();
        if (itemInHand == null || itemInHand.getType() == Material.AIR) {
            Messenger.send(sender, "itemtool.noItem");
            return;
        }

        // Change the lore.
        final ItemMeta meta = itemInHand.getItemMeta();
        final List<String> lore = meta.hasLore() ? meta.getLore() : new ArrayList<>();
        lore.add(loreLine);
        meta.setLore(lore);
        itemInHand.setItemMeta(meta);
        Messenger.send(sender, "itemtool.applied");
    }
}
