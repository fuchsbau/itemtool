package de.fuchspfoten.itemtool.command;

import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.fuchslib.PlayerHelper;
import de.fuchspfoten.fuchslib.command.LeafCommand;
import de.fuchspfoten.itemtool.ItemToolPlugin;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 * /i load KEY loads the item with the given key.
 */
public class ItemToolLoadCommand extends LeafCommand {

    /**
     * Constructor.
     */
    protected ItemToolLoadCommand() {
        super("itemtool.help.load", "itemtool.use");
        Messenger.register("itemtool.notFound");
        Messenger.register("itemtool.received");
    }

    @Override
    protected void invoke(final CommandSender sender, final String[] args) {
        final Player player = PlayerHelper.ensurePlayer(sender);
        if (player == null) {
            return;
        }

        // Syntax check.
        if (args.length != 1) {
            Messenger.send(sender, "itemtool.help.load");
            return;
        }
        final String key = args[0].toLowerCase();
        final ItemStack item = ItemToolPlugin.getSelf().getItemStorage().load(key);

        if (item == null) {
            Messenger.send(sender, "itemtool.notFound");
            return;
        }

        player.getInventory().addItem(item);
        Messenger.send(sender, "itemtool.received");
    }
}
