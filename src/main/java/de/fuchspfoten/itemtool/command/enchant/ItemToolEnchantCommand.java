package de.fuchspfoten.itemtool.command.enchant;

import de.fuchspfoten.fuchslib.command.TreeCommand;

/**
 * /i enchant manages enchantments.
 */
public class ItemToolEnchantCommand extends TreeCommand {

    /**
     * Constructor.
     */
    public ItemToolEnchantCommand() {
        super("itemtool.help.enchant.self");
        addSubCommand("add", new ItemToolEnchantAddCommand());
        addSubCommand("list", new ItemToolEnchantListCommand());
        addSubCommand("remove", new ItemToolEnchantRemoveCommand());
        addSubCommand("show", new ItemToolEnchantShowCommand());
    }
}
