package de.fuchspfoten.itemtool.command;

import de.fuchspfoten.fuchslib.command.TreeCommand;
import de.fuchspfoten.itemtool.command.custom.ItemToolCustomCommand;
import de.fuchspfoten.itemtool.command.enchant.ItemToolEnchantCommand;
import de.fuchspfoten.itemtool.command.lore.ItemToolLoreCommand;
import de.fuchspfoten.itemtool.command.recipe.ItemToolRecipeCommand;

/**
 * Item management command.
 */
public class ItemToolCommand extends TreeCommand {

    /**
     * Constructor.
     */
    public ItemToolCommand() {
        super(null);
        addSubCommand("custom", new ItemToolCustomCommand());
        addSubCommand("damage", new ItemToolDamageCommand());
        addSubCommand("enchant", new ItemToolEnchantCommand());
        addSubCommand("get", new ItemToolGetCommand());
        addSubCommand("load", new ItemToolLoadCommand());
        addSubCommand("lore", new ItemToolLoreCommand());
        addSubCommand("more", new ItemToolMoreCommand());
        addSubCommand("name", new ItemToolNameCommand());
        addSubCommand("recipe", new ItemToolRecipeCommand());
        addSubCommand("remove", new ItemToolRemoveCommand());
        addSubCommand("save", new ItemToolSaveCommand());
    }
}
