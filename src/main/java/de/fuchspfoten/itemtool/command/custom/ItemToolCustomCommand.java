package de.fuchspfoten.itemtool.command.custom;

import de.fuchspfoten.fuchslib.command.TreeCommand;

/**
 * /i custom manages {@link de.fuchspfoten.fuchslib.item.CustomItems}.
 */
public class ItemToolCustomCommand extends TreeCommand {

    /**
     * Constructor.
     */
    public ItemToolCustomCommand() {
        super("itemtool.help.custom.self");
        addSubCommand("get", new ItemToolCustomGetCommand());
        addSubCommand("list", new ItemToolCustomListCommand());
    }
}
