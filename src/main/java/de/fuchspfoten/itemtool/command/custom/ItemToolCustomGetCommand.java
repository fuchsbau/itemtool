package de.fuchspfoten.itemtool.command.custom;

import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.fuchslib.PlayerHelper;
import de.fuchspfoten.fuchslib.command.LeafCommand;
import de.fuchspfoten.fuchslib.item.CustomItems;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Arrays;

/**
 * /i custom get QUERY acquires the queried {@link de.fuchspfoten.fuchslib.item.CustomItems} item.
 */
public class ItemToolCustomGetCommand extends LeafCommand {

    /**
     * Constructor.
     */
    protected ItemToolCustomGetCommand() {
        super("itemtool.help.custom.get", "itemtool.use");
        Messenger.register("itemtool.notFound");
        Messenger.register("itemtool.received");
    }

    @Override
    protected void invoke(final CommandSender sender, final String[] args) {
        final Player player = PlayerHelper.ensurePlayer(sender);
        if (player == null) {
            return;
        }

        // Syntax check.
        if (args.length != 1) {
            Messenger.send(sender, "itemtool.help.custom.get");
            return;
        }

        // Find the queried item.
        final CustomItems customItem = Arrays.stream(CustomItems.values())
                .filter(c -> c.name().equalsIgnoreCase(args[0]))
                .findAny().orElse(null);
        if (customItem == null) {
            Messenger.send(sender, "itemtool.notFound");
            return;
        }

        // Acquire the item.
        player.getInventory().addItem(customItem.getFactory().instance());
        Messenger.send(sender, "itemtool.received");
    }
}
