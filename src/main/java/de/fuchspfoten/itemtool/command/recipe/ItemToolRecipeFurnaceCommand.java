package de.fuchspfoten.itemtool.command.recipe;

import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.fuchslib.command.LeafCommand;
import de.fuchspfoten.itemtool.ItemStorage;
import de.fuchspfoten.itemtool.ItemToolPlugin;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.inventory.FurnaceRecipe;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.Recipe;

/**
 * /i recipe furnace KEY IN OUT EXP registers a furnace recipe.
 */
public class ItemToolRecipeFurnaceCommand extends LeafCommand {

    /**
     * Constructor.
     */
    protected ItemToolRecipeFurnaceCommand() {
        super("itemtool.help.recipe.furnace", "itemtool.use");
        Messenger.register("itemtool.recipe.notFound");
        Messenger.register("itemtool.recipe.registered");
    }

    @Override
    protected void invoke(final CommandSender sender, final String[] args) {
        // Syntax check.
        if (args.length != 4) {
            Messenger.send(sender, "itemtool.help.recipe.furnace");
            return;
        }
        final String key = args[0].toLowerCase();
        final String keyIn = args[1].toLowerCase();
        final String keyOut = args[2].toLowerCase();
        final float experience;
        try {
            experience = Float.parseFloat(args[3]);
            if (experience <= 0.0f) {
                return;
            }
        } catch (final NumberFormatException ex) {
            return;
        }

        // Fetch the ingredients and the result.
        final ItemStorage storage = ItemToolPlugin.getSelf().getItemStorage();
        final ItemStack in = storage.load(keyIn);
        final ItemStack out = storage.load(keyOut);
        if (in == null || out == null) {
            Messenger.send(sender, "itemtool.recipe.notFound");
            return;
        }

        // Store the recipe.
        final Recipe recipe = new FurnaceRecipe(out, in.getData(), experience);
        Bukkit.addRecipe(recipe);
        ItemToolPlugin.getSelf().getCraftingStorage().store(key, recipe);
        ItemToolPlugin.getSelf().getCraftingStorage().save();
        Messenger.send(sender, "itemtool.recipe.registered");
    }
}
