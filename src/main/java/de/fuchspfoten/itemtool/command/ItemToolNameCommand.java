package de.fuchspfoten.itemtool.command;

import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.fuchslib.PlayerHelper;
import de.fuchspfoten.fuchslib.command.LeafCommand;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

/**
 * /i name NAME sets the item name.
 */
public class ItemToolNameCommand extends LeafCommand {

    /**
     * Constructor.
     */
    protected ItemToolNameCommand() {
        super("itemtool.help.name", "itemtool.use");
        Messenger.register("itemtool.applied");
        Messenger.register("itemtool.noItem");
    }

    @Override
    protected void invoke(final CommandSender sender, final String[] args) {
        final Player player = PlayerHelper.ensurePlayer(sender);
        if (player == null) {
            return;
        }

        // Syntax check.
        if (args.length < 1) {
            Messenger.send(sender, "itemtool.help.name");
            return;
        }

        // Parse the new name.
        final StringBuilder nameBuilder = new StringBuilder();
        for (final String arg : args) {
            nameBuilder.append(arg).append(' ');
        }
        final String newName = ChatColor.translateAlternateColorCodes('&', nameBuilder.toString().trim());

        // Get the item to edit.
        final ItemStack itemInHand = player.getInventory().getItemInMainHand();
        if (itemInHand == null || itemInHand.getType() == Material.AIR) {
            Messenger.send(sender, "itemtool.noItem");
            return;
        }

        // Change the name.
        final ItemMeta meta = itemInHand.getItemMeta();
        meta.setDisplayName(newName);
        itemInHand.setItemMeta(meta);
        Messenger.send(sender, "itemtool.applied");
    }
}
