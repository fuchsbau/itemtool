package de.fuchspfoten.itemtool.command.recipe;

import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.fuchslib.command.LeafCommand;
import de.fuchspfoten.itemtool.ItemToolPlugin;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;

/**
 * /i recipe reset resets the crafting recipes and applies changes.
 */
public class ItemToolRecipeResetCommand extends LeafCommand {

    /**
     * Constructor.
     */
    protected ItemToolRecipeResetCommand() {
        super("itemtool.help.recipe.reset", "itemtool.use");
        Messenger.register("itemtool.recipe.reset");
    }

    @Override
    protected void invoke(final CommandSender sender, final String[] args) {
        // Reset the recipes.
        Bukkit.resetRecipes();

        // Reload custom recipes.
        ItemToolPlugin.getSelf().getCraftingStorage().loadAllRecipes();

        Messenger.send(sender, "itemtool.recipe.reset");
    }
}
