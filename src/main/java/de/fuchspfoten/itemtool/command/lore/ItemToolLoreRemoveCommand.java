package de.fuchspfoten.itemtool.command.lore;

import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.fuchslib.PlayerHelper;
import de.fuchspfoten.fuchslib.command.LeafCommand;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

/**
 * /i lore remove [pos] removes a lore line.
 */
public class ItemToolLoreRemoveCommand extends LeafCommand {

    /**
     * Constructor.
     */
    protected ItemToolLoreRemoveCommand() {
        super("itemtool.help.lore.remove", "itemtool.use");
        Messenger.register("itemtool.applied");
        Messenger.register("itemtool.noItem");
        Messenger.register("itemtool.lore.invalidIndex");
    }

    @Override
    protected void invoke(final CommandSender sender, final String[] args) {
        final Player player = PlayerHelper.ensurePlayer(sender);
        if (player == null) {
            return;
        }

        // Syntax check.
        if (args.length < 1) {
            Messenger.send(sender, "itemtool.help.lore.remove");
            return;
        }
        final int editPos;
        try {
            editPos = Integer.parseInt(args[0]);
        } catch (final NumberFormatException ex) {
            return;
        }

        // Get the item to edit.
        final ItemStack itemInHand = player.getInventory().getItemInMainHand();
        if (itemInHand == null || itemInHand.getType() == Material.AIR) {
            Messenger.send(sender, "itemtool.noItem");
            return;
        }

        // Check the lore.
        final ItemMeta meta = itemInHand.getItemMeta();
        final List<String> lore = meta.hasLore() ? meta.getLore() : new ArrayList<>();
        if (editPos < 0 || editPos >= lore.size()) {
            Messenger.send(sender, "itemtool.lore.invalidIndex");
            return;
        }

        // Change the lore.
        lore.remove(editPos);
        meta.setLore(lore);
        itemInHand.setItemMeta(meta);
        Messenger.send(sender, "itemtool.applied");
    }
}
