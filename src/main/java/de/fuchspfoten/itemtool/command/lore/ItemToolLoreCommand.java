package de.fuchspfoten.itemtool.command.lore;

import de.fuchspfoten.fuchslib.command.TreeCommand;

/**
 * /i lore manages lore.
 */
public class ItemToolLoreCommand extends TreeCommand {

    /**
     * Constructor.
     */
    public ItemToolLoreCommand() {
        super("itemtool.help.lore.self");
        addSubCommand("add", new ItemToolLoreAddCommand());
        addSubCommand("edit", new ItemToolLoreEditCommand());
        addSubCommand("list", new ItemToolLoreListCommand());
        addSubCommand("remove", new ItemToolLoreRemoveCommand());
    }
}
