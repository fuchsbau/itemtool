package de.fuchspfoten.itemtool.command.recipe;

import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.fuchslib.command.LeafCommand;
import de.fuchspfoten.itemtool.ItemStorage;
import de.fuchspfoten.itemtool.ItemToolPlugin;
import org.bukkit.Bukkit;
import org.bukkit.NamespacedKey;
import org.bukkit.command.CommandSender;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapelessRecipe;

import java.util.ArrayList;
import java.util.Collection;

/**
 * /i recipe shapeless KEY OUT IN... registers a shapeless recipe.
 */
public class ItemToolRecipeShapelessCommand extends LeafCommand {

    /**
     * Constructor.
     */
    protected ItemToolRecipeShapelessCommand() {
        super("itemtool.help.recipe.shapeless", "itemtool.use");
        Messenger.register("itemtool.recipe.notFound");
        Messenger.register("itemtool.recipe.registered");
    }

    @Override
    protected void invoke(final CommandSender sender, final String[] args) {
        // Syntax check.
        if (args.length < 3) {
            Messenger.send(sender, "itemtool.help.recipe.shapeless");
            return;
        }
        final String key = args[0].toLowerCase();
        final String keyOut = args[1].toLowerCase();

        // Fetch the ingredients and the result.
        final ItemStorage storage = ItemToolPlugin.getSelf().getItemStorage();
        final Collection<ItemStack> ingredients = new ArrayList<>();
        for (int i = 2; i < args.length; i++) {
            final String keyIn = args[i].toLowerCase();
            final ItemStack ingredient = storage.load(keyIn);
            if (ingredient == null) {
                Messenger.send(sender, "itemtool.recipe.notFound");
                return;
            }
            ingredients.add(ingredient);
        }
        final ItemStack out = storage.load(keyOut);
        if (out == null) {
            Messenger.send(sender, "itemtool.recipe.notFound");
            return;
        }

        // Store the recipe.
        final ShapelessRecipe recipe = new ShapelessRecipe(new NamespacedKey(ItemToolPlugin.getSelf(), key), out);
        ingredients.forEach(i -> recipe.addIngredient(i.getData()));
        Bukkit.addRecipe(recipe);
        ItemToolPlugin.getSelf().getCraftingStorage().store(key, recipe);
        ItemToolPlugin.getSelf().getCraftingStorage().save();
        Messenger.send(sender, "itemtool.recipe.registered");
    }
}
