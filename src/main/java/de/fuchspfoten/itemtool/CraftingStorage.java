package de.fuchspfoten.itemtool;

import de.fuchspfoten.fuchslib.data.DataFile;
import org.bukkit.Bukkit;
import org.bukkit.NamespacedKey;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.inventory.FurnaceRecipe;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.Recipe;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.ShapelessRecipe;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.Map.Entry;

/**
 * The crafting storage provides a persistent keyed storage of recipes.
 */
public class CraftingStorage {

    /**
     * The storage backend file.
     */
    private final DataFile storageBackend;

    /**
     * Constructor.
     *
     * @param dataFolder The data folder in which the storage file is created.
     */
    public CraftingStorage(final File dataFolder) {
        storageBackend = new DataFile(new File(dataFolder, "crafting.yml"), new CraftingStorageVersionUpdater());
    }

    /**
     * Saves the crafting storage.
     */
    public void save() {
        storageBackend.save(CraftingStorageVersionUpdater.VERSION);
    }

    /**
     * Stores a recipe in the crafting storage.
     *
     * @param key    The key under which the recipe is stored.
     * @param recipe The recipe that is stored.
     */
    public void store(final String key, final Recipe recipe) {
        final YamlConfiguration config = storageBackend.getStorage();

        if (recipe instanceof FurnaceRecipe) {
            final FurnaceRecipe furnaceRecipe = (FurnaceRecipe) recipe;
            config.set(key + "._type", "furnace");
            config.set(key + "._exp", furnaceRecipe.getExperience());
            config.set(key + "._in", furnaceRecipe.getInput());
            config.set(key + "._out", furnaceRecipe.getResult());
        } else if (recipe instanceof ShapelessRecipe) {
            final ShapelessRecipe shapelessRecipe = (ShapelessRecipe) recipe;
            config.set(key + "._type", "shapeless");
            config.set(key + "._out", shapelessRecipe.getResult());
            config.set(key + "._key", shapelessRecipe.getKey().getKey());

            int i = 0;
            for (final ItemStack ingredient : shapelessRecipe.getIngredientList()) {
                final String subKey = key + "._in.i" + i;
                config.set(subKey, ingredient);
                i++;
            }
        } else if (recipe instanceof ShapedRecipe) {
            final ShapedRecipe shapedRecipe = (ShapedRecipe) recipe;
            config.set(key + "._type", "shaped");
            config.set(key + "._out", shapedRecipe.getResult());
            config.set(key + "._key", shapedRecipe.getKey().getKey());
            config.set(key + "._shape", Arrays.asList(shapedRecipe.getShape()));

            for (final Entry<Character, ItemStack> entry : shapedRecipe.getIngredientMap().entrySet()) {
                if (entry.getValue() == null) {
                    continue;
                }
                final String subKey = key + "._in." + entry.getKey();
                config.set(subKey, entry.getValue());
            }
        } else {
            throw new IllegalArgumentException("unsupported recipe " + recipe);
        }
    }

    /**
     * Removes a recipe from the item storage.
     *
     * @param key The key which is to be removed.
     */
    public void remove(final String key) {
        storageBackend.getStorage().set(key, null);
    }

    /**
     * Loads all stored recipes and registers them.
     */
    public void loadAllRecipes() {
        loadAllRecipes(storageBackend.getStorage(), "");
    }

    /**
     * Loads all recipes in the given section and registers them.
     *
     * @param section The section.
     * @param fullKey The full key.
     */
    private void loadAllRecipes(final ConfigurationSection section, final String fullKey) {
        for (final String key : section.getKeys(false)) {
            if (key.startsWith("_")) {
                continue;
            }
            if (!section.isConfigurationSection(key)) {
                throw new IllegalStateException("traversal may only be called for section containing sections");
            }
            final String combinedKey = fullKey.isEmpty() ? key : fullKey + '.' + key;

            final ConfigurationSection subSection = section.getConfigurationSection(key);
            if (subSection.contains("_type")) {
                Bukkit.addRecipe(load(combinedKey));
            } else {
                loadAllRecipes(subSection, combinedKey);
            }
        }
    }

    /**
     * Loads a recipe from the crafting storage.
     *
     * @param key The key of the recipe to load.
     * @return The loaded recipe, or null.
     */
    private Recipe load(final String key) {
        final YamlConfiguration config = storageBackend.getStorage();
        final String type = config.getString(key + "._type");

        if (type.equals("furnace")) {
            return new FurnaceRecipe(config.getItemStack(key + "._out"),
                    config.getItemStack(key + "._in").getData(), (float) config.getDouble(key + "._exp"));
        }

        if (type.equals("shapeless")) {
            final ShapelessRecipe recipe = new ShapelessRecipe(
                    new NamespacedKey(ItemToolPlugin.getSelf(), config.getString(key + "._key")),
                    config.getItemStack(key + "._out"));

            // Ingredients use anonymous numbered keys.
            final ConfigurationSection ingredients = config.getConfigurationSection(key + "._in");
            for (final String subKey : ingredients.getKeys(false)) {
                recipe.addIngredient(ingredients.getItemStack(subKey).getData());
            }
            return recipe;
        }

        if (type.equals("shaped")) {
            final ShapedRecipe recipe = new ShapedRecipe(
                    new NamespacedKey(ItemToolPlugin.getSelf(), config.getString(key + "._key")),
                    config.getItemStack(key + "._out"));
            final List<String> shape = config.getStringList(key + "._shape");
            recipe.shape(shape.toArray(new String[0]));

            // Ingredients use the characters as keys.
            final ConfigurationSection ingredients = config.getConfigurationSection(key + "._in");
            for (final String charKey : ingredients.getKeys(false)) {
                recipe.setIngredient(charKey.charAt(0), ingredients.getItemStack(charKey).getData());
            }
            return recipe;
        }

        throw new IllegalArgumentException("invalid key");
    }
}
