package de.fuchspfoten.itemtool.command.enchant;

import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.fuchslib.command.LeafCommand;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * /i enchant list lists available enchantments.
 */
public class ItemToolEnchantListCommand extends LeafCommand {

    /**
     * Constructor.
     */
    protected ItemToolEnchantListCommand() {
        super("itemtool.help.enchant.list", "itemtool.use");
        Messenger.register("itemtool.enchant.list");
    }

    @Override
    protected void invoke(final CommandSender sender, final String[] args) {
        // Show the available enchantments.
        Messenger.send(sender, "itemtool.enchant.list", String.join(", ",
                Arrays.stream(Enchantment.values())
                        .map(Enchantment::getName)
                        .map(String::toLowerCase)
                        .collect(Collectors.toList())));
    }
}
