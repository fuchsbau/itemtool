package de.fuchspfoten.itemtool.command.enchant;

import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.fuchslib.PlayerHelper;
import de.fuchspfoten.fuchslib.command.LeafCommand;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.Map.Entry;

/**
 * /i enchant show shows the enchantments of the current item.
 */
public class ItemToolEnchantShowCommand extends LeafCommand {

    /**
     * Constructor.
     */
    protected ItemToolEnchantShowCommand() {
        super("itemtool.help.enchant.show", "itemtool.use");
        Messenger.register("itemtool.noItem");
        Messenger.register("itemtool.enchant.showHeader");
        Messenger.register("itemtool.enchant.show");
    }

    @Override
    protected void invoke(final CommandSender sender, final String[] args) {
        final Player player = PlayerHelper.ensurePlayer(sender);
        if (player == null) {
            return;
        }

        // Get the item to edit.
        final ItemStack itemInHand = player.getInventory().getItemInMainHand();
        if (itemInHand == null || itemInHand.getType() == Material.AIR) {
            Messenger.send(sender, "itemtool.noItem");
            return;
        }

        // Show the enchantments on the item.
        Messenger.send(sender, "itemtool.enchant.showHeader");
        for (final Entry<Enchantment, Integer> entry : itemInHand.getEnchantments().entrySet()) {
            Messenger.send(sender, "itemtool.enchant.show", entry.getKey().getName().toLowerCase(),
                    entry.getValue());
        }
    }
}
