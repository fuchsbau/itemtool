package de.fuchspfoten.itemtool;

import de.fuchspfoten.fuchslib.data.DataFile;
import org.bukkit.inventory.ItemStack;

import java.io.File;

/**
 * The item storage provides a persistent string-key based storage of items.
 */
public class ItemStorage {

    /**
     * The storage backend file.
     */
    private final DataFile storageBackend;

    /**
     * Constructor.
     *
     * @param dataFolder The data folder in which the storage file is created.
     */
    public ItemStorage(final File dataFolder) {
        storageBackend = new DataFile(new File(dataFolder, "storage.yml"), new ItemStorageVersionUpdater());
    }

    /**
     * Saves the item storage.
     */
    public void save() {
        storageBackend.save(ItemStorageVersionUpdater.VERSION);
    }

    /**
     * Stores an item in the item storage.
     *
     * @param key  The key under which the item is stored.
     * @param item The item that is stored.
     */
    public void store(final String key, final ItemStack item) {
        storageBackend.getStorage().set(key, item);
    }

    /**
     * Removes an item from the item storage.
     *
     * @param key The key which is to be removed.
     */
    public void remove(final String key) {
        storageBackend.getStorage().set(key, null);
    }

    /**
     * Loads an item from the item storage.
     *
     * @param key The key of the item to load.
     * @return The loaded item, or null.
     */
    public ItemStack load(final String key) {
        return storageBackend.getStorage().getItemStack(key);
    }
}
