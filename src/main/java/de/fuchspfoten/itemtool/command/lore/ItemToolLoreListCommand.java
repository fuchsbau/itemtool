package de.fuchspfoten.itemtool.command.lore;

import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.fuchslib.PlayerHelper;
import de.fuchspfoten.fuchslib.command.LeafCommand;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

/**
 * /i lore list lists lore on an item.
 */
public class ItemToolLoreListCommand extends LeafCommand {

    /**
     * Constructor.
     */
    protected ItemToolLoreListCommand() {
        super("itemtool.help.lore.list", "itemtool.use");
        Messenger.register("itemtool.lore.list");
        Messenger.register("itemtool.lore.none");
        Messenger.register("itemtool.noItem");
    }

    @Override
    protected void invoke(final CommandSender sender, final String[] args) {
        final Player player = PlayerHelper.ensurePlayer(sender);
        if (player == null) {
            return;
        }

        // Get the item to edit.
        final ItemStack itemInHand = player.getInventory().getItemInMainHand();
        if (itemInHand == null || itemInHand.getType() == Material.AIR) {
            Messenger.send(sender, "itemtool.noItem");
            return;
        }

        // Print the lore lines.
        final ItemMeta meta = itemInHand.getItemMeta();
        if (meta.hasLore()) {
            int line = 0;
            for (final String lore : itemInHand.getItemMeta().getLore()) {
                Messenger.send(sender, "itemtool.lore.list", line++, lore);
            }
        } else {
            Messenger.send(sender, "itemtool.lore.none");
        }
    }
}
