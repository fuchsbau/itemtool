package de.fuchspfoten.itemtool.command.lore;

import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.fuchslib.PlayerHelper;
import de.fuchspfoten.fuchslib.command.LeafCommand;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

/**
 * /i lore edit [pos] LINE edits a lore line.
 */
public class ItemToolLoreEditCommand extends LeafCommand {

    /**
     * Constructor.
     */
    protected ItemToolLoreEditCommand() {
        super("itemtool.help.lore.edit", "itemtool.use");
        Messenger.register("itemtool.applied");
        Messenger.register("itemtool.noItem");
        Messenger.register("itemtool.lore.invalidIndex");
    }

    @Override
    protected void invoke(final CommandSender sender, final String[] args) {
        final Player player = PlayerHelper.ensurePlayer(sender);
        if (player == null) {
            return;
        }

        // Syntax check.
        if (args.length < 2) {
            Messenger.send(sender, "itemtool.help.lore.edit");
            return;
        }
        final int editPos;
        try {
            editPos = Integer.parseInt(args[0]);
        } catch (final NumberFormatException ex) {
            return;
        }

        // Get the item to edit.
        final ItemStack itemInHand = player.getInventory().getItemInMainHand();
        if (itemInHand == null || itemInHand.getType() == Material.AIR) {
            Messenger.send(sender, "itemtool.noItem");
            return;
        }

        // Check the lore.
        final ItemMeta meta = itemInHand.getItemMeta();
        final List<String> lore = meta.hasLore() ? meta.getLore() : new ArrayList<>();
        if (editPos < 0 || editPos >= lore.size()) {
            Messenger.send(sender, "itemtool.lore.invalidIndex");
            return;
        }

        // Parse the new lore line.
        final StringBuilder loreBuilder = new StringBuilder();
        final String[] lineArgs = new String[args.length - 1];
        System.arraycopy(args, 1, lineArgs, 0, lineArgs.length);
        for (final String arg : lineArgs) {
            loreBuilder.append(arg).append(' ');
        }
        final String loreLine = ChatColor.translateAlternateColorCodes('&', loreBuilder.toString().trim());

        // Change the lore.
        lore.set(editPos, loreLine);
        meta.setLore(lore);
        itemInHand.setItemMeta(meta);
        Messenger.send(sender, "itemtool.applied");
    }
}
